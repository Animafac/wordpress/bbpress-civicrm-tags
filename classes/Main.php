<?php
/**
 * Main class.
 */

namespace BbpressCivicrmTags;

use CivicrmApi\Tag;

/**
 * Class containing main actions and filters.
 */
class Main
{
    /**
     * Save tags when the form has been submitted.
     * @param  integer $topicId bbPress topic ID
     * @return void
     */
    public static function saveFields($topicId = 0)
    {
        foreach (Tag::getAllTagsets() as $parent) {
            $inputName = 'bbp_civicrm_tag_'.$parent->get('id');
            if (isset($_POST[$inputName])) {
                update_post_meta($topicId, $inputName, $_POST[$inputName]);
            }
        }
    }

    /**
     * Add new fields to the bbPress form.
     */
    public static function addFields()
    {
        $topicId = bbp_get_topic_id();
        foreach (Tag::getAllTagsets() as $parent) {
            $inputName = 'bbp_civicrm_tag_'.$parent->get('id');
            $value = get_post_meta($topicId, $inputName, true);

            echo '<label for="'.$inputName.'">'.$parent->get('name').'</label><br>
                <select id="'.$inputName.'" name="'.$inputName.'">
                <option></option>';
            foreach ($parent->getChildren() as $tag) {
                $id = $tag->get('id');
                echo '<option ';
                if ($id == $value) {
                    echo ' selected ';
                }
                echo 'value="'.$id.'">'.$tag->get('name').'</option>';
            }
            echo '</select><br/>';
        }
    }

    public static function displayFields()
    {
        $topicId = bbp_get_topic_id();
        foreach (Tag::getAllTagsets() as $parent) {
            $inputName = 'bbp_civicrm_tag_'.$parent->get('id');
            $value = get_post_meta($topicId, $inputName, true);
            if (!empty($value)) {
                $tag = new Tag($value);
                echo '<b>'.$parent->get('name').'</b>: '.$tag->get('name');
            }
        }
    }
}
