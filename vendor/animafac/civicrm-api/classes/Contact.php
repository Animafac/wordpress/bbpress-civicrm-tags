<?php
/**
 * Contact class.
 */

namespace CivicrmApi;

use Exception;
use stdClass;

/**
 * Manage CiviCRM contacts.
 */
class Contact extends ApiObject
{
    /**
     * Contact constructor.
     *
     * @param int $id CiviCRM contact ID
     */
    public function __construct($id)
    {
        parent::__construct('contact_id', $id);
    }

    /**
     * Get a CiviCRM website type object.
     *
     * @param string $name Type name
     * @return stdClass
     */
    private function getWebsiteType($name)
    {
        foreach ($this->api->getOptions('Website', 'website_type_id') as $type) {
            if ($type->value == $name) {
                return $type;
            }
        }
        throw new Exception("Can't find this website type: ".$name);
    }

    /**
     * Get all available website types
     * @return string[]
     */
    public function getWebsiteTypes()
    {
        $return = [];
        foreach ($this->api->getOptions('Website', 'website_type_id') as $type) {
            $return[] = $type->value;
        }
        return $return;
    }

    /**
     * Get one of the contact's websites.
     *
     * @param string $type Website type
     *
     * @return string URL
     */
    public function getWebsite($type)
    {
        $websiteType = $this->getWebsiteType($type);
        try {
            return $this->api->getSingle(
                'Website',
                [
                    'contact_id' => $this->get('id'),
                    'website_type_id' => $websiteType->key
                ]
            )->url;
        } catch (ApiException $e) {
            return '';
        }
    }
}
