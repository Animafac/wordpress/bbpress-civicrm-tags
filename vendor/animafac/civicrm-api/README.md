# CiviCRM PHP API

This library allows you to easily use the [CiviCRM PHP API](https://docs.civicrm.org/dev/en/latest/api/usage/#php-classapiphp).

## Example

```php
use CivicrmApi\Api;
use CivicrmApi\Contact;

require_once __DIR__.'/vendor/autoload.php';
require_once 'path/to/civicrm/api/class.api.php';

Api::$path = '/path/to/civicrm/config/';

$contact = new Contact(['contact_id' => $id]);
echo $contact->get('display_name');
```
